"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.lambdaHandler = void 0;
const db_1 = require("./db");
const Bcrypt = require("bcryptjs");
const Jwt = require("jsonwebtoken");
const lambdaHandler = async (event) => {
    try {
        // Get an instance of our database
        const db = await (0, db_1.dbManager)();
        const request = JSON.parse(event.body);
        // In a more elaborated signup process there would be more checks, like password length / security and email validity
        if (!request.email || !request.password) {
            return {
                statusCode: 400,
                body: `Missing username or password`
            };
        }
        const user = await db.collection("users").findOne({ email: request.email });
        if (!Bcrypt.compareSync(request.password, user.password)) {
            return {
                statusCode: 401,
                body: `Username and password does not match`
            };
        }
        const token = Jwt.sign({ user_id: user._id }, "secrettoken", {
            expiresIn: "2h",
        });
        return {
            statusCode: 200,
            body: token
        };
    }
    catch (exception) {
        return {
            statusCode: 500,
            body: exception.message
        };
    }
};
exports.lambdaHandler = lambdaHandler;
//# sourceMappingURL=login.js.map