"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.lambdaHandler = void 0;
const db_1 = require("./db");
const Bcrypt = require("bcryptjs");
const lambdaHandler = async (event) => {
    try {
        // Get an instance of our database
        const db = await (0, db_1.dbManager)();
        const request = JSON.parse(event.body);
        // In a more elaborated signup process there would be more checks, like password length / security and email validity
        if (!request.username || !request.email || !request.password) {
            return {
                statusCode: 400,
                body: `Missing username or email or password`
            };
        }
        await db.collection("users").insertOne({ username: request.username, email: request.email, password: Bcrypt.hashSync(request.password, 10) });
        return {
            statusCode: 200,
            body: `User Registered`
        };
    }
    catch (exception) {
        return {
            statusCode: 500,
            body: exception.message
        };
    }
};
exports.lambdaHandler = lambdaHandler;
//# sourceMappingURL=signup.js.map