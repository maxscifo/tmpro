"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.lambdaHandler = void 0;
const aws = require("aws-sdk");
const snsClient = new aws.SNS({ region: 'us-east-1' });
const Mongo = require("mongodb");
const db_1 = require("./db");
const lambdaHandler = async (event) => {
    try {
        const request = JSON.parse(event.body);
        if (!request.message || !request.receiverEmail) {
            return {
                statusCode: 200,
                body: `Missing message or receiver email`
            };
        }
        // Get an instance of our database
        const db = await (0, db_1.dbManager)();
        const user = await db.collection("users").findOne({ email: request.receiverEmail });
        if (!user) {
            return {
                statusCode: 200,
                body: `Receiver not found`
            };
        }
        await db.collection("messages").insertOne({ sender: new Mongo.ObjectId(event.requestContext.authorizer.lambda.user_id), receiver: user._id, message: request.message });
        // call send email
        const userEmail = {
            email: {
                DataType: "String",
                StringValue: user.email
            }
        };
        var params = {
            Message: request.message,
            TopicArn: "arn:aws:sns:us-east-1:103065688000:sendEmail",
            MessageAttributes: userEmail
        };
        await snsClient.publish(params).promise();
        return {
            statusCode: 200,
            body: `Message sent successfuly`
        };
    }
    catch (exception) {
        return {
            statusCode: 500,
            body: exception.message
        };
    }
};
exports.lambdaHandler = lambdaHandler;
//# sourceMappingURL=sendMessage.js.map