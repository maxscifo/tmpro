"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.lambdaHandler = void 0;
const aws = require("aws-sdk");
var ses = new aws.SES({ region: "us-east-1" });
const lambdaHandler = async (event) => {
    try {
        if (!event.Records[0].Sns.MessageAttributes["email"]) {
            return {
                statusCode: 200,
                body: `Missing receiver email`
            };
        }
        // very quick and dirty way for populating the email since we are using a findone and we can only have one email per time, but ideally this would be an endpoint for send more 
        var params = {
            Destination: {
                ToAddresses: [event.Records[0].Sns.MessageAttributes["email"].Value]
            },
            Message: {
                Body: {
                    Html: {
                        Charset: "UTF-8",
                        Data: event.Records[0].Sns.Message,
                    }
                },
                Subject: { Data: "TM Pro test" },
            },
            Source: "massimilianoscifo+noreply@gmail.com" //"test@team94155.testinator.com",
        };
        await ses.sendEmail(params).promise();
        return {
            statusCode: 200,
            body: `Message sent successfuly`
        };
    }
    catch (exception) {
        return {
            statusCode: 500,
            body: exception.message
        };
    }
};
exports.lambdaHandler = lambdaHandler;
//# sourceMappingURL=sendEmail.js.map