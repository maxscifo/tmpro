"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dbManager = void 0;
const mongodb_1 = require("mongodb");
async function dbManager() {
    const MONGODB_URI = "mongodb+srv://cluster0.qlzxd.mongodb.net/test?authSource=$external&authMechanism=MONGODB-AWS&retryWrites=true&w=majority";
    let cachedDb = null;
    if (cachedDb) {
        return cachedDb;
    }
    // Connect to our MongoDB database hosted on MongoDB Atlas
    const client = await mongodb_1.MongoClient.connect(MONGODB_URI);
    // Specify which database we want to use
    const db = await client.db("tmpro");
    cachedDb = db;
    return db;
}
exports.dbManager = dbManager;
//# sourceMappingURL=db.js.map