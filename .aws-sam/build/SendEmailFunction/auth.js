"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.lambdaHandler = void 0;
const Jwt = require("jsonwebtoken");
const lambdaHandler = async (event) => {
    let response = {
        statusCode: 401,
        isAuthorized: false,
        context: {}
    };
    // try to validate token
    // the string should come from a centralized config / environment variable
    const user = Jwt.verify(event.headers.authorization, "secrettoken");
    if (user) {
        response = {
            statusCode: 200,
            isAuthorized: true,
            context: {
                user_id: user.user_id,
            }
        };
    }
    return response;
};
exports.lambdaHandler = lambdaHandler;
//# sourceMappingURL=auth.js.map