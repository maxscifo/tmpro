import { 
    APIGatewayProxyEvent, 
    APIGatewayProxyResult,
    APIGatewayProxyResultV2
  } from "aws-lambda";
  
import { dbManager } from './db';
  
export const lambdaHandler = async ( event: APIGatewayProxyEvent) => {
    try {
        let limit = parseInt(event.queryStringParameters.limit) ? parseInt(event.queryStringParameters.limit) : 10; 
        let page = parseInt(event.queryStringParameters.page) ? parseInt(event.queryStringParameters.page) : 1;
        let skips = limit * (page - 1);
        if(!event.queryStringParameters.receiverEmail){
            return {
                statusCode: 200,
                body: `Missing receiver email`
            }
        }

        // Get an instance of our database
        const db = await dbManager();

        const user = await db.collection("users").findOne({email : event.queryStringParameters.receiverEmail});

        if(!user._id){
            return {
                statusCode: 200,
                body: `User not found`
            }
        }

        const messages = await db.collection("messages").find({receiver : user._id}).skip(skips).limit(limit).toArray();
        if(!messages){
            return {
                statusCode: 200,
                body: `No Message found`
            }
        }

        const senderIds = [];
        const returnMessages = [];
        for(var message of messages){
            if(senderIds.indexOf(message.sender) == -1){
                senderIds.push(message.sender);
            }
            returnMessages.push({
                message: message.message,
                sender: message.sender
            })
        }

        // in this way we are iterating on the response 1 time more but we save at least 10 queries
        const senders = await db.collection("users").find({_id : { $in : senderIds}}).toArray();
        for(var sender of senders){
            returnMessages.find((message) => {
                if(message.sender == sender._id.toString()){
                    message.sender = sender.username
                }
            });
        }

        return {
            statusCode: 200,
            body: JSON.stringify(returnMessages)
        }
    } catch (exception){
        return {
        statusCode: 500,
        body: exception.message
        }
    }
}