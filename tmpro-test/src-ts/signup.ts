import { 
    APIGatewayProxyEvent, 
    APIGatewayProxyResult 
  } from "aws-lambda";
  
  import { dbManager } from './db';
  import * as Bcrypt from 'bcryptjs';
  
  export const lambdaHandler = async (
    event: APIGatewayProxyEvent
  ): Promise<APIGatewayProxyResult> => {
      try {
          // Get an instance of our database
          const db = await dbManager();

          const request = JSON.parse(event.body);
          
          // In a more elaborated signup process there would be more checks, like password length / security and email validity
          if(!request.username || !request.email || !request.password){
              return {
                  statusCode: 400,
                  body: `Missing username or email or password`
              }
          }
      
          await db.collection("users").insertOne({ username : request.username, email : request.email, password : Bcrypt.hashSync(request.password, 10)});
        
          return {
            statusCode: 200,
            body: `User Registered`
          }
      } catch(exception){
        return {
            statusCode: 500,
            body: exception.message
          }
      }
  }