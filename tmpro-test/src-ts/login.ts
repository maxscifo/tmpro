import { 
    APIGatewayProxyEvent, 
    APIGatewayProxyResult 
  } from "aws-lambda";
  
  import { dbManager } from './db';
  import * as Bcrypt from 'bcryptjs';
  import * as Jwt from 'jsonwebtoken';
  
export const lambdaHandler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    try {
        // Get an instance of our database
        const db = await dbManager();

        const request = JSON.parse(event.body);
        
        // In a more elaborated signup process there would be more checks, like password length / security and email validity
        if(!request.email || !request.password){
            return {
                statusCode: 400,
                body: `Missing username or password`
            }
        }
    
        const user = await db.collection("users").findOne({ email : request.email });
        
        if(!Bcrypt.compareSync(request.password, user.password)){
            return {
                statusCode: 401,
                body: `Username and password does not match`
            }    
        }

        const token = Jwt.sign(
            { user_id: user._id },
            "secrettoken",
            {
              expiresIn: "2h",
            }
        );

        return {
            statusCode: 200,
            body: token
        }
    } catch(exception){
        return {
            statusCode: 500,
            body: exception.message
        }
    }
}