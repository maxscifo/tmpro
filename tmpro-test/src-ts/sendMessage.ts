import { 
  APIGatewayProxyEvent, 
  APIGatewayProxyResult
} from "aws-lambda";

import * as aws from "aws-sdk";
import { MessageAttributeMap } from "aws-sdk/clients/sns";

const snsClient = new aws.SNS({region : 'us-east-1'})

import * as Mongo from 'mongodb';
import { dbManager } from './db';

export const lambdaHandler = async ( event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  try {
    const request = JSON.parse(event.body);
    if(!request.message || !request.receiverEmail){
      return {
        statusCode: 200,
        body: `Missing message or receiver email`
      }
    }
    
    // Get an instance of our database
    const db = await dbManager();

    const user = await db.collection("users").findOne({email : request.receiverEmail});

    if(!user){
      return {
        statusCode: 200,
        body: `Receiver not found`
      }
    }

    await db.collection("messages").insertOne({ sender: new Mongo.ObjectId(event.requestContext.authorizer.lambda.user_id), receiver : user._id, message : request.message});
    // call send email
    const userEmail : MessageAttributeMap = {
      email : {
        DataType : "String",
        StringValue : user.email
      }
    }
    var params = {
      Message: request.message,
      TopicArn: "arn:aws:sns:us-east-1:103065688000:sendEmail", // config or var env
      MessageAttributes : userEmail
    };

    await snsClient.publish(params).promise();

    return {
      statusCode: 200,
      body: `Message sent successfuly`
    }
  } catch (exception){
    return {
      statusCode: 500,
      body: exception.message
    }
  }
}