import { MongoClient } from "mongodb";

export async function dbManager () {
    const MONGODB_URI = "mongodb+srv://cluster0.qlzxd.mongodb.net/test?authSource=$external&authMechanism=MONGODB-AWS&retryWrites=true&w=majority"
    let cachedDb : any = null;

	  if (cachedDb) {
      return cachedDb;
    }
    // Connect to our MongoDB database hosted on MongoDB Atlas
    const client = await MongoClient.connect(MONGODB_URI);
    // Specify which database we want to use
    const db = await client.db("tmpro");
    cachedDb = db;
    return db;
}