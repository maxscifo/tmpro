import * as Jwt from 'jsonwebtoken';

export const lambdaHandler = async (event: { headers: { authorization: string; }; })  => { 
    let response = {
        statusCode: 401,
        isAuthorized: false,
        context : {}
    };

    // try to validate token
    // the string should come from a centralized config / environment variable
    const user = Jwt.verify(event.headers.authorization, "secrettoken")
    if (user) {
        response = {
            statusCode: 200,
            isAuthorized: true,
            context: {
                user_id: (<{user_id : string}>user).user_id,
            }
        };
    }

    return response;
};